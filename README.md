# Karolis Rusenas

* __Currently lives in:__ London
* __Email:__              karolis.rusenas@gmail.com
* __Telephone number:__   07479 702264

## Summary

> Full Stack developer. Master in Python, experienced in Go, JavaScript (React.js), Expert in MongoDB (deployment, async usage, performance optimisations)
data processing, web services (creating and consuming APIs), Django/Tornado frameworks,
building 12-factor applications, automated deployment with Docker. Also, have lots of experience with OpenStack!

GitHub: https://github.com/rusenask

## Work experience

* June 2015 - Present | __Software developer/consultant__ | Open Credo

 > Lead developer, designer for service virtualization https://github.com/SpectoLabs/mirage software using Tornado framework (Python) with React (JavaScript/JSX), 
 creating micro-service proxies in Go. Enabling fully automated deployment with Docker containers. Currently re-imagining service virtualization and dependency management - creating new breed of backing services to speed up development and testing.
 Also, working on some side projects like creating custom dashboards that consume Google Analytics API, Salesforce. 

* December 2014 - Present | __CTO/Developer/Engineer__ | Unfuzz

 > Co-founder, working as a software developer, choosing right technologies for the job. Creating software for some really really fast data gathering. Also, working as a DevOps to automate deployment, monitoring.

* March 2014 - June 2015 | __Python developer/IaaS engineer__ | Barclays

 > Designing and developing new systems and services for internal cloud based on OpenStack.
Main projects that I developed:
 - System for automated OpenStack deployment (connecting to servers through IPMI, mounting images, installing and configuring OpenStack)
 - System for global management of all OpenStack environment, capacity monitoring, information aggregation.
 - API management solution for OpenStack to enforce business rules.
 - Adding custom business logic for virtual machine provisioning across internal cloud.


 * September 2013 - March 2014 | __Service Analyst__ | Barclays

 >Working with large network management systems such as Spectrum, BMC BladeLogic Network Automation, NetQoS, NetDoctor.
Activities focused on :
Data mining & analysis - asset information collection, discrepancies identification, refinery.
SharePoint development - creating partly automated Team Sites for various projects.


* March 2012 - July 2013 | __Engineer__ | Dicto Citius

 >Network management, configuration, improvement. GPS tracking solutions ( both Iridium based and 3G ). Surveillance - NVR, IP cameras and much more.
New product search, analysis, sales and support. Project requirement analysis and specification, revenue forecasting.

* June 2011 - March 2013 | __IT Specialist__ | Lintel

 >Troubleshooting network problem, small network configuration, line tests, IPTV configuration.

## Education

* Master's degree, Information Systems Engineering (Kaunas University of Technology)

 > Mostly focusing on designing & building reliable information systems based on different and complex data sources such as LinkedData or also known as semantic web, testing. Master's degree project - end-to-end cloud deployment and environment management software.


* Bachelor's degree, Computer Science and Engineering (Kaunas University of Technology)

 > Wide range of activities, starting from C++ to Java, internet of things. Bachelor project - indoor navigation system based on WLAN (used mathematical models to calculate signal propagation and precompile signal strength in each building location)

## Hobbies

* Learning new technologies
* Gym
* Cooking
* Pub
